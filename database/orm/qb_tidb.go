/**
 * Created by goland
 * User: adam_wang
 * Date: 2024/8/11
 * Time: 上午9:17
 */

package orm

type TiDBQueryBuilder struct {
	MySqlQueryBuilder
	tokens []string
}
