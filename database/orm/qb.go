/**
 * Created by goland
 * User: adam_wang
 * Date: 2024/8/11
 * Time: 上午1:29
 */

package orm

import (
	"errors"
	"github.com/beego/beego/v2/client/orm"
)

type Where struct {
	Field     string
	Condition string
	Value     interface{}
}

type QueryBuilder interface {
	orm.QueryBuilder
	WhereSqlBuilder(where []Where) QueryBuilder
	GetWhereParams() []interface{}
}

// NewQueryBuilder return the QueryBuilder
func NewQueryBuilder(driver string) (qb QueryBuilder, err error) {
	if driver == "mysql" {
		qb = new(MySqlQueryBuilder)
	} else if driver == "tidb" {
		qb = new(TiDBQueryBuilder)
	} else if driver == "postgres" {
		qb = new(PostgresQueryBuilder)
	} else if driver == "sqlite" {
		err = errors.New("sqlite query builder is not supported yet")
	} else {
		err = errors.New("unknown driver for query builder")
	}
	return
}
